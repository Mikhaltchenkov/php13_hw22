<?php
if (isset($_GET['test'])) {
    if (!file_exists($_GET['test'])) {
        header("HTTP/1.0 404 Not Found");
        exit;
    }
} elseif (isset($_POST['test'])) {
    if (!file_exists($_POST['test'])) {
        header("HTTP/1.0 404 Not Found");
        exit;
    }
}
?>
<html>
<head>
    <title>Тестирование</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
<div class="container">
    <div class="masthead">
        <h3 class="text-muted">Система тестирования</h3>
        <ul class="nav nav-justified">
            <li><a href="index.php">Главная</a></li>
            <li><a href="admin.php">Админка</a></li>
            <li class="active"><a href="list.php">Тесты</a></li>
        </ul>
    </div>
    <?php
    if (isset($_GET['test'])) {
        $test_string = file_get_contents($_GET['test']) or die('Не возможно прочитать файл!');
        $test = json_decode($test_string, true);
        echo '<div class="container"><div class="page-header"><h1>' . $test['name'] . '</h1></div>';
    ?>
    <form action="test.php" method="post">
        <input type="hidden" name="test" value="<?= $_GET['test'] ?>">
        <div class="row form-group">
            <div class="col-md-12">
            <input type="text" name="student" id="student" class="form-control" placeholder="Введите свое имя" required>
            </div>
        </div>
        <?php
        $test_num = 1;
        foreach ($test['tests'] as $question) {
            echo '<div class=\'jumbotron\'>';
            echo '<h2>' . $test_num . '. ' . $question['question'] . '</h2>';
            $answer_num = 1;
            foreach ($question['answers'] as $answer) {
                echo "<label><input type='radio' name='answer$test_num' value='$answer_num'> $answer</label><br/>";
                $answer_num++;
            }
            $test_num++;
            echo '</div>';
        }
        ?>
        <input type="submit" name="btn_send" class="btn btn-primary" value="Оправить на проверку">
    </form>
    <?php
    } else {
        if (isset($_POST['test'])) {
            $test_string = file_get_contents($_POST['test']) or die('Не возможно прочитать файл!');
            $test = json_decode($test_string, true);
            $passing = $test['passing'];
            $test_num = 1;
            $right_answers = 0;
            echo '<div class="container"><div class="page-header"><h1>Результаты тестирования:</h1></div><ol>';
            foreach ($test['tests'] as $question) {
                $note_text = '<span class="bg-danger">НЕ ПРАВИЛЬНЫЙ ОТВЕТ</span>';
                if ($question['right'] == $_POST['answer' . $test_num]) {
                    $note_text = '<span class="bg-success">ПРАВИЛЬНЫЙ ОТВЕТ</span>';
                    $right_answers++;
                }
                echo '<li>' . $question['question'] . ' - ' . $note_text . '</li>';
                $test_num++;
            }
            echo '</ol>';
            $score = 100 / ($test_num - 1) * $right_answers;
            if ($score >= $passing) {
                echo "<p>Поздравляем! Вы сдали тест, набрав $score баллов. Ваш сертификат:</p>";
                echo "<div class='row'>";
                echo '<img src="certificate.php?test=' . $_POST['test'] . '&student=' . $_POST['student'] . '&score=' . $score . '" class="col-md-8 col-md-offset-2">';
                echo "</div>";
            } else {
                echo '<p>Сожалеем, но Вы не сдали тест, попробуйте снова.</p>';
            }
            echo '</div>';
        } else {
            echo '<div class="panel panel-danger"><div class="panel-heading"><h4>Ошибка</h4></div><div class="panel-body">Неправильный запрос</div></div>';
        }
    }
    ?>
</div>
<div id="footer">
    <div class="container"><p class="text-muted"> ©2017, Михальченков Дмитрий</p></div>
</div>
</body>
</html>
