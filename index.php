<html>
<head>
    <title>Система тестирования</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
<div class="container">
    <div class="masthead">
        <h3 class="text-muted">Система тестирования</h3>
        <ul class="nav nav-justified">
            <li class="active"><a href="index.php">Главная</a></li>
            <li><a href="admin.php">Админка</a></li>
            <li><a href="list.php">Тесты</a></li>
        </ul>
    </div>
    <div class="page-header">
        <h1>Домашняя работа к занятию «Обработка форм» </h1>
    </div>
    <p>Данная работа реализована в рамках выполнения домашнего задания в Он-лайн университете "Нетология"</p>
</div>
<div id="footer">
    <div class="container"><p class="text-muted"> ©2017, Михальченков Дмитрий</p></div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
