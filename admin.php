<?php
$message = '';
if (isset($_POST['btn_send'])) {
    if (isset($_FILES['testFile']['name']) &&
        !empty($_FILES['testFile']['name']) &&
        preg_match("/\.json$/i", $_FILES['testFile']['name']) != NULL
    ) {
        $destination = date('Ymd_His') . '.json';
        if (move_uploaded_file($_FILES['testFile']['tmp_name'], $destination)) {
            header("Location: list.php");
            exit;
        } else {
            $message = '<div class="panel panel-danger"><div class="panel-heading"><h4>Ошибка</h4></div><div class="panel-body">Ошибка загрузки файла</div></div>';
        }
    } else {
        $message = '<div class="panel panel-danger"><div class="panel-heading"><h4>Ошибка</h4></div><div class="panel-body">Файл не загружен или имеет не верный формат!</div></div>';
    }
}
?>
<html>
<head>
    <title>Система тестирования</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
<div class="container">
    <div class="masthead">
        <h3 class="text-muted">Система тестирования</h3>
        <ul class="nav nav-justified">
            <li><a href="index.php">Главная</a></li>
            <li class="active"><a href="admin.php">Админка</a></li>
            <li><a href="list.php">Тесты</a></li>
        </ul>
    </div>
    <div class="page-header">
        <h1>Загрузка тестов </h1>
    </div>
    <?= $message ?>
    <form action="" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="testFile">Загрузить файл с тестами:</label>
            <input class="form-control" type="file" name="testFile">
        </div>

        <div class="form-group">
            <input class="btn btn-primary" name="btn_send" type="submit" value="Отправить">
        </div>
    </form>
</div>
<div id="footer">
    <div class="container"><p class="text-muted"> ©2017, Михальченков Дмитрий</p></div>
</div>
</body>
</html>

