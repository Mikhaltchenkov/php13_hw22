<html>
<head>
    <title>Система тестирования</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
<div class="container">
    <div class="masthead">
        <h3 class="text-muted">Система тестирования</h3>
        <ul class="nav nav-justified">
            <li><a href="index.php">Главная</a></li>
            <li><a href="admin.php">Админка</a></li>
            <li class="active"><a href="list.php">Тесты</a></li>
        </ul>
    </div>
    <div class="page-header">
        <h1>Список тестов</h1>
    </div>
    <?php
    $filenames = glob("*.json");
    if (count($filenames) > 0) {
        echo "<ul>";
        foreach ($filenames as $filename) {
            echo "<li><a href='test.php?test=$filename'>$filename</a></li>";
        }
        echo "</ul>";
    } else {
        echo '<div class="panel panel-info"><div class="panel-heading"><h4>Внимание</h4></div><div class="panel-body">Файлы тестов не найдены, загрузите их через <a href=\'admin.php\'>админку</a></div></div>';
    }
    ?>
</div>
<div id="footer">
    <div class="container"><p class="text-muted"> ©2017, Михальченков Дмитрий</p></div>
</div>
</body>
</html>
