<?php
define('CERTIFICATE_FILE', __DIR__ . '/assets/certificate.jpg');
define('FONT_FILE', __DIR__ . '/assets/Roboto-Regular.ttf');
define('CERTIFICATE_WIDTH', 800);
define('CERTIFICATE_HEIGHT', 600);
define('CERTIFICATE_TEXT', 'успешно прошел тестирование по программе:');

$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
if (isset($_GET['test']) && isset($_GET['student']) && isset($_GET['score'])) {
    if (!file_exists($_GET['test'])) {
        header("$protocol 404 Not Found");
        exit;
    } else {
        $testString = file_get_contents($_GET['test']) or die('Не возможно прочитать файл!');
        $test = json_decode($testString, true);
        $testName = $test['name'];
        $score = $_GET['score'];
        $student = $_GET['student'];
    }
} else {
    header("$protocol 400 Bad Request");
    exit;
}
header("Content-Type: image/png");


list($width, $height, $type) = getimagesize(CERTIFICATE_FILE) or die("Cannot Initialize new GD image stream");
$image = imagecreatetruecolor(CERTIFICATE_WIDTH, CERTIFICATE_HEIGHT);
$imageSource = imagecreatefromjpeg(CERTIFICATE_FILE) or die("Cannot Initialize new GD image stream");
imagecopyresampled($image, $imageSource, 0, 0, 0, 0, CERTIFICATE_WIDTH, CERTIFICATE_HEIGHT, $width, $height);

$black = imagecolorallocate($image, 0, 0, 0);

//название теста
$box = imagettfbbox(18,0,FONT_FILE,CERTIFICATE_TEXT);
$textWidth = $box[2] - $box[0];
imagettftext($image, 18, 0, (CERTIFICATE_WIDTH - $textWidth) / 2, 290, $black, FONT_FILE, CERTIFICATE_TEXT);


//название теста
$box = imagettfbbox(24,0,FONT_FILE,$testName);
$textWidth = $box[2] - $box[0];
imagettftext($image, 24, 0, (CERTIFICATE_WIDTH - $textWidth) / 2, 350, $black, FONT_FILE, $testName);

//ФИО студента
$box = imagettfbbox(24,0,FONT_FILE, $student);
$textWidth = $box[2] - $box[0];
imagettftext($image, 24, 0, (CERTIFICATE_WIDTH - $textWidth) / 2, 230, $black, FONT_FILE, $student);

// Оценка
imagettftext($image, 20, 0, 100, 500, $black, FONT_FILE, "$score Баллов");

imagettftext($image, 20, 0, 580, 500, $black, FONT_FILE, date('d.m.Y'));

imagepng($image);

imagedestroy($image);
imagedestroy($imageSource);
